#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	int a, b, ans, score = 0;
	// Used for CTF-style exploit
	char *unsafe_buffer = malloc(64);
	char buf[32];
	memset(buf, 0, 32);
	srand(time(0));

	puts("Welcome to VulnMath\nWhere your wildest shells can come true\n");

	for (int i = 0; i < 6; i++) {
		// range: 1-20
		// if 0 is in range then the script fails when entering '%N$X' input
		a = (rand() % 19)+1;
		b = (rand() % 19)+1;
		printf("What is %d * %d?\n> ", a, b);

		read(0, unsafe_buffer, 32);
		memcpy(buf, unsafe_buffer, 32);
		ans = atoi(buf);

		if (ans == (a*b)) {
			puts("Correct! +5 points");
			score += 5;
		} else {
			puts("Incorrect!");
			// The meat of the challenge
			printf(buf);
		}
		puts("");
	}

	printf("Final Score: %d\n", score);
	puts("Thanks for playing!");
	// The key to the challenge
	free(unsafe_buffer);

    return 0;
}
