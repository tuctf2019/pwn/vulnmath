# vulnmath

Desc: `Enter the belly of the beast and emerge victorious.`

Architecture: x86

Given files:

* vulnmath
* libc.so.6

Hints:

* What all can printf do? Can you leverage it's capabilites?

Flag: `TUCTF{I_w45_w4rn3d_4b0u7_pr1n7f..._bu7_I_d1dn'7_l1573n}`
